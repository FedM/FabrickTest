package com.fabrick.test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fabrick.test.domain.Transaction;
import com.fabrick.test.dto.AccountBalance;
import com.fabrick.test.exception.CustomError;
import com.fabrick.test.response.ApiResponseGeneric;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RunWith(SpringRunner.class) 
@SpringBootTest
@AutoConfigureMockMvc
class TestApplicationTests {
	
	
	private static Integer ID_ACCOUNT = 14537780;
	
	@Autowired 
    private MockMvc mockMvc; 
	
	
    @Test 
    public void getBalanceTest() throws Exception { 
    	String path = "/api/balance/" + ID_ACCOUNT;
        MvcResult mvcResult = this.mockMvc 
                .perform(get(path)) 
                .andDo(print()) 
                .andExpect(status().isOk())
                .andExpect(content()
                	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        
         String content = mvcResult.getResponse().getContentAsString();
         Type ApiResponseGenericType= new TypeToken<ApiResponseGeneric<AccountBalance>>(){}.getType();
         ApiResponseGeneric<AccountBalance> response = new Gson().fromJson(content, ApiResponseGenericType);
         List<CustomError> errors = response.getErrors();
         assertNotEquals(response.getPayload(), null);
         assertNotEquals(errors, null);
         if(errors != null)
           assertTrue(errors.size() == 0);
    }
    
    @Test 
    public void getTransactionTest() throws Exception { 
    	String path = "/api/transaction/" + ID_ACCOUNT;
    	String fromDate = "2019-04-01";
    	String toDate = "2020-04-01";
        MvcResult mvcResult = this.mockMvc 
                .perform(get(path) 
                .param("fromAccountingDate", fromDate)
                .param("toAccountingDate", toDate) 
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        
         String content = mvcResult.getResponse().getContentAsString();
         Type transactionListType = new TypeToken<ApiResponseGeneric<List<Transaction>>>(){}.getType();
         ApiResponseGeneric<List<Transaction>> response = new Gson().fromJson(content, transactionListType);
         List<CustomError> errors = response.getErrors();
         assertNotEquals(response.getPayload(), null);
         assertNotEquals(errors, null);
         if(errors != null)
           assertTrue(errors.size() == 0);
         
    }
    
    @Test 
    public void makeMoneyTransfer() throws Exception { 
    	String path = "/api/moneyTransfer/" + ID_ACCOUNT;
    	
    	String json = "{\r\n  \"creditor\": {\r\n    \"name\": \"John Doe\",\r\n    \"account\": {\r\n      \"accountCode\": \"IT66C0100503382000000218020\",\r\n      \"bicCode\": \"SELBIT2BXXX\"\r\n    },\r\n    \"address\": {\r\n      \"address\": \"via Mario Carucci 89\",\r\n      \"city\": \"Roma\",\r\n      \"countryCode\": \"IT\"\r\n    }\r\n  },\r\n  \"executionDate\": \"2022-08-08\",\r\n  \"uri\": \"REMITTANCE_INFORMATION\",\r\n  \"description\": \"Payment invoice 75/2022\",\r\n  \"amount\": 1,\r\n  \"currency\": \"EUR\",\r\n  \"isUrgent\": false,\r\n  \"isInstant\": false,\r\n  \"feeType\": \"SHA\",\r\n  \"feeAccountId\": null\r\n  \r\n}";
   
        this.mockMvc 
                .perform(post(path).content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
         
    }
}
