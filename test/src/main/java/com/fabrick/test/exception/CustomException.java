package com.fabrick.test.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8912607477097727499L;
	
	private String message;
	private String code;
	
	public CustomException(String message, String code) {
		this.message = message;
		this.code = code;
	}
	
	public CustomException(String message) {
		this.message = message;
	}
	
   
}
