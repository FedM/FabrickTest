package com.fabrick.test.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.function.client.WebClientResponseException;


@RestControllerAdvice
public class GlobalExceptionHandler {

	private final Logger logger = LogManager.getLogger();

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ApiCallError> handleNotFoundException(HttpServletRequest request,
			ResourceNotFoundException ex) {
		logger.error("ResourceNotFoundException {}\n", request.getRequestURI(), ex);

		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ApiCallError("Not found exception", ex.getMessage()));
	}


	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<ApiCallError> handleMissingServletRequestParameterException(HttpServletRequest request,
			MissingServletRequestParameterException ex) {
		logger.error("handleMissingServletRequestParameterException {}\n", request.getRequestURI(), ex);

		return ResponseEntity.badRequest().body(new ApiCallError("Missing request parameter", ex.getMessage()));
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApiCallError> handleInternalServerError(HttpServletRequest request, Exception ex) {
		logger.error("handleInternalServerError {}\n", request.getRequestURI(), ex);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ApiCallError("Internal server error", ex.getMessage()));
	}
	
	
	@ExceptionHandler(WebClientResponseException.class)
	public ResponseEntity<ApiError> handleHttpClient(HttpServletRequest request, WebClientResponseException ex) {
		logger.error("handleHttpClientError {}\n", request.getRequestURI(), ex);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(new ApiError( ex.getResponseBodyAsString()));
	}
	

}
