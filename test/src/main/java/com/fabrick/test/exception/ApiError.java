package com.fabrick.test.exception;

import java.util.List;

import com.fabrick.test.response.ApiResponse;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {
	
	private List<CustomError>errors;
	private String status; 
	private Object payload; // PATH


	public ApiError(String bodyAsString) {
		Gson gson = new Gson();
		ApiResponse response = gson.fromJson(bodyAsString, ApiResponse.class);
		if (response != null) {
			this.status = response.getStatus();
			this.payload = response.getPayload();
			this.errors = response.getErrors();
		}
	}

	
}
