package com.fabrick.test.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomError {
	
	private String code;
	private String description;
	private String params;

}
