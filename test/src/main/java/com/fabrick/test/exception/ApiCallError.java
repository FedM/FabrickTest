package com.fabrick.test.exception;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiCallError {
	private String reasonPhrase; // HTTP STATUS REASON PHRASE
	private String message; // BREAF ERROR DESCRIPTION
	private String instance; // PATHs

	public ApiCallError(String reasonPhrase, String message) {
		this.reasonPhrase = reasonPhrase;
		this.message = message;
	}

	public ApiCallError(String reasonPhrase, List<Map<String, String>> details) {
		this.reasonPhrase = reasonPhrase;
		this.message = "[";

		details.forEach(detail -> {
			this.message += convertWithStream(detail);
		});

		this.message += "]";
	}

	public ApiCallError(String reasonPhrase, Map<String, String> detail) {
		this.reasonPhrase = reasonPhrase;
		this.message += convertWithStream(detail);
	}
	

	public static String convertWithStream(Map<String, String> map) {
		String mapAsString = map.keySet().stream().map(key -> key + " = " + map.get(key))
				.collect(Collectors.joining(", ", "{ ", " }"));
		return mapAsString;
	}
}