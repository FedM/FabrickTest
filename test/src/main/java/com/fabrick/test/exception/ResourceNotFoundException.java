package com.fabrick.test.exception;

public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException(String id) {
		super(String.format("Resource with id <%s> not found", id));
	}
}
