package com.fabrick.test.confg;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

@Component
public class AppConfig {
	
	private static final String API_KEY = "API-Key";
	private static final String AUTH_SCHEMA = "Auth-Schema";
	
	@Value("${baseUrl}")
	String baseUrl;
	
	@Value("${authSchema}")
	String authSchema;
	
	@Value("${apiKey}")
	String apiKey;
	
	@Value("${timeout}")
	Integer timeout;
	
	@Bean
	public WebClient getWebClient(WebClient.Builder webClientBuilder) {
	    return webClientBuilder
	    	 .clientConnector(new ReactorClientHttpConnector(getHttpClient()))
	        .baseUrl(baseUrl)
	        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
	        .defaultHeader(API_KEY, apiKey)
            .defaultHeader(AUTH_SCHEMA, authSchema)
	        .build();
	}
	
	private HttpClient getHttpClient() {
        return HttpClient.create()
        		  .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeout)
        		  .responseTimeout(Duration.ofMillis(timeout))
        		  .doOnConnected(conn -> 
        		    conn.addHandlerLast(new ReadTimeoutHandler(timeout, TimeUnit.MILLISECONDS))
        		      .addHandlerLast(new WriteTimeoutHandler(timeout, TimeUnit.MILLISECONDS)));
    }

}
