package com.fabrick.test.controller;


import java.math.BigDecimal;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fabrick.test.domain.Transaction;
import com.fabrick.test.dto.AccountBalance;
import com.fabrick.test.dto.AccountBalanceDto;
import com.fabrick.test.dto.AccountTransactionDto;
import com.fabrick.test.dto.AccountTransactionListDto;
import com.fabrick.test.dto.MoneyTransfer;
import com.fabrick.test.exception.CustomException;
import com.fabrick.test.response.ApiResponse;
import com.fabrick.test.response.ApiResponseGeneric;
import com.fabrick.test.service.ApiService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api")
public class ApiController {
	

	private static final String NO_RESOURCE = "Problemi nel recupero della risora";
	private static final String OK = "OK";
	
	private final ApiService apiService;
  
	/**
	 * Get balance by accoundId
	 * 
	 * @param idAccount
	 * @return
	 * @throws CustomException
	 */
	@GetMapping("/balance/{idAccount}")
	public ResponseEntity<ApiResponseGeneric<AccountBalance>> getBalance(@PathVariable Integer idAccount) throws CustomException {
		ResponseEntity<AccountBalanceDto> response =  apiService.getBalance(idAccount);
		if(response != null) {
			AccountBalanceDto body = response.getBody();
			if(body != null) {
				 AccountBalance balance = body.getPayload();
				 if(balance != null)
				    return new ResponseEntity<ApiResponseGeneric<AccountBalance>>(new ApiResponseGeneric<AccountBalance>(balance, OK), HttpStatus.OK);
			}
		}
		throw new CustomException(NO_RESOURCE);	  
	}
		
	/**
	 * Get transactions by accountId and params date.
	 * 
	 * @param idAccount
	 * @param fromAccountingDate
	 * @param toAccountingDate
	 * @return
	 * @throws CustomException
	 */
	@GetMapping("/transaction/{idAccount}")
	public ResponseEntity<ApiResponseGeneric<List<Transaction>>> getTransaction(@PathVariable Integer idAccount, 
			@RequestParam String fromAccountingDate, @RequestParam String toAccountingDate ) throws CustomException {
		 ResponseEntity<AccountTransactionDto> accountTransaction = apiService.getTransaction(idAccount, fromAccountingDate, toAccountingDate );
		 if(accountTransaction != null) {
			 AccountTransactionDto body = accountTransaction.getBody();
			 if(body != null) {
				 AccountTransactionListDto payload =  body.getPayload();
				 if(payload != null) {
		          List<Transaction> transactions = (List<Transaction>) payload.getList();
		          apiService.insertTransaction(transactions);
		          return new ResponseEntity<ApiResponseGeneric<List<Transaction>>>(new ApiResponseGeneric<List<Transaction>>(transactions, OK), HttpStatus.OK);
				 }
			 }
		 }
		 throw new CustomException(NO_RESOURCE);
	}
	
	/**
	 * Make money transfer by idAccount
	 * 
	 * @param idAccount
	 * @param moneyTransfer
	 * @return
	 * @throws CustomException
	 */
	@PostMapping("/moneyTransfer/{idAccount}")
	public ResponseEntity<ApiResponse> maketMoneyTransfer(@PathVariable Integer idAccount, 
			@RequestBody MoneyTransfer moneyTransfer)  {
		return apiService.makeMoneyTransfer(idAccount, moneyTransfer);
	}
	
}
