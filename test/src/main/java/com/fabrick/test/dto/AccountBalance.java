package com.fabrick.test.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AccountBalance {
	
	private Date date;
	private BigDecimal balance;
	private BigDecimal availableBalance;
	private String currency;
	

}
