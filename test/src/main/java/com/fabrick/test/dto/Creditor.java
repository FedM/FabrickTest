package com.fabrick.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Creditor {
	
	private String name;
	private Account account;
	private Adress address;
	

}
