package com.fabrick.test.dto;

import java.util.List;

import com.fabrick.test.domain.Transaction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountTransactionListDto {
	
	private List<Transaction> list;

}
