package com.fabrick.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Adress {
	
	private String address;
	private String city;
	private String countryCode;
	
}
