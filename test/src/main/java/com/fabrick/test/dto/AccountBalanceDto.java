package com.fabrick.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountBalanceDto {
	
	private AccountBalance payload;

}
