package com.fabrick.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {
	
	 private String accountCode;
	 private String bicCode;

}
