package com.fabrick.test.response;

import java.util.ArrayList;
import java.util.List;

import com.fabrick.test.exception.CustomError;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse {
	
	private List<CustomError>errors = new ArrayList<>();
	private String status = "";
	private Object payload;

}
