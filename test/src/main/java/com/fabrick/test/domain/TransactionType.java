package com.fabrick.test.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "transaction_type")
public class TransactionType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
    private Long idType;
	
	private String enumeration;
	
	@Column(name="value_type")
	private String value;
	
	@JsonBackReference
	@OneToMany(mappedBy="type", cascade = CascadeType.ALL)
	private List<Transaction> transactions;

}
