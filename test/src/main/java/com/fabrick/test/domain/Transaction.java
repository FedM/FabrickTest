package com.fabrick.test.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "transaction_account")
public class Transaction {
	
	@Id
	private Long transactionId;
	
	private String operationId;
	
	private BigDecimal amount;
	
	private Date accountingDate;
	
	private Date valueDate;
	
    private String currency;
    
    private String description;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="idType", nullable=false)
    private TransactionType type;
}
