package com.fabrick.test.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fabrick.test.domain.Transaction;
import com.fabrick.test.dto.AccountBalanceDto;
import com.fabrick.test.dto.AccountTransactionDto;
import com.fabrick.test.dto.MoneyTransfer;
import com.fabrick.test.repository.TransactionRepository;
import com.fabrick.test.response.ApiResponse;
import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Service
public class ApiServiceImpl implements ApiService {
	
	private static Logger log = LogManager.getLogger(ApiServiceImpl.class);
	
	
    private final WebClient webClient;
    private final TransactionRepository transactionRepository;
	

	@Override
    public ResponseEntity<AccountBalanceDto> getBalance(Integer accountId) {
		
		log.info("Call getBalance");
  
    	return webClient.get()
                .uri("/api/gbs/banking/v4.0/accounts/{accountId}/balance", accountId)
                .retrieve()
                .toEntity(AccountBalanceDto.class)
                .block();
                
    }
	
	@Override
    public ResponseEntity<AccountTransactionDto> getTransaction(Integer accountId, String fromDate, String toDate) {
		
		log.info("Call Transaction");

		String requestPath = "/api/gbs/banking/v4.0/accounts/{accountId}/transactions";
    	return webClient.get()
                .uri(builder -> builder.path(requestPath).queryParam("fromAccountingDate", fromDate).queryParam("toAccountingDate", toDate).build(accountId))
                .retrieve()
                .toEntity(AccountTransactionDto.class)
                .block();
                
    }
	
	@Override
	public ResponseEntity<ApiResponse> makeMoneyTransfer(Integer accountId, MoneyTransfer moneyTransfer) {
		
		log.info("Make money transfer");
		
		String requestPath = "/api/gbs/banking/v4.0/accounts/{accountId}/payments/money-transfers";
		return webClient			
				 .post()
				.uri(requestPath, accountId)
				.header("X-Time-Zone", "Europe/Rome")
				.body(BodyInserters.fromValue(new Gson().toJson(moneyTransfer)))
				.retrieve()
				.toEntity(ApiResponse.class)
				.block();
		
	}
	
	
	@Override
	public void insertTransaction(List<Transaction> transactions) {
		log.info("Insert Transactions in db");
		for(Transaction transaction: transactions)
		  transactionRepository.save(transaction);
	}
	
	
	


}
