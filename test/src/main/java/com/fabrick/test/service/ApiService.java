package com.fabrick.test.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fabrick.test.domain.Transaction;
import com.fabrick.test.dto.AccountBalance;
import com.fabrick.test.dto.AccountBalanceDto;
import com.fabrick.test.dto.AccountTransactionDto;
import com.fabrick.test.dto.MoneyTransfer;
import com.fabrick.test.response.ApiResponse;
import com.fabrick.test.response.ApiResponseGeneric;


public interface ApiService {

	/**
	 * Get balance by Id
	 * 
	 * @param accountId
	 * @return
	 */
	ResponseEntity<AccountBalanceDto> getBalance(Integer accountId);

	/**
	 * Get transactions by accountId.
	 * 
	 * @param accountId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	ResponseEntity<AccountTransactionDto> getTransaction(Integer accountId, String fromDate, String toDate);

	/**
	 * Insert transaction in db.
	 * 
	 * @param transactions
	 */
	void insertTransaction(List<Transaction> transactions);

	/**
	 * Make money transfer.
	 * 
	 * @param accountId
	 * @param moneyTransfer
	 * @return
	 */
	ResponseEntity<ApiResponse> makeMoneyTransfer(Integer accountId, MoneyTransfer moneyTransfer);

	

}
